#include "mqlogsav.h"
#include <cassert>

#include <stdio.h>
#include <io.h>
#include <fstream>
#include <ctime>
#include <algorithm>

#include <iostream>

using namespace std;

MqLogSAV::MqLogSAV(string fileName, string path, unsigned int fileCout, unsigned int fileSize)
    :mFileName(fileName)
    ,mPath(path)
    ,mFilesCout(fileCout)
    ,mFileSize(fileSize)
{
    assert(!mPath.empty());

    if(mPath.at(mPath.size() - 1)  != '/')
        mPath += '/';
}

bool MqLogSAV::log(const string& message)
{
    auto name = mFileName + "_" + to_string(mCurrentFileNumber) + ".log";

    //  access
    //    0	Существование файла
    //    1	Исполняемость файла
    //    2	Доступность для записи
    //    3	Доступность для чтения
    //    4	Доступность для чтения/записи
    if(access(string(mPath + name).c_str(), 0) != 0)
    {
        ofstream fs(mPath + name, std::ios::app);
        if(!fs.is_open())
            return false;
        fs.close();
    }
    else if(access(string(mPath + name).c_str(), 4) != 0)
    {
        return false;
    }


    string msg = message;
    fixMsg(msg);

    string date;
    {
        time_t seconds = time(NULL);
        //deprecated//устаревшие функции
        std::tm* timeInfo = localtime(&seconds);
        date = asctime(timeInfo);
        if(!date.empty())
            date.resize(date.size() - 1);
    }

    auto writeMode = std::ios::app;
    uint64_t fileSize = 0;
    {
        std::ifstream fs(mPath + name);
        fs.seekg(0, std::ios::end);
        fileSize = fs.tellg();
    }

    if(fileSize + msg.size() + date.size() > mFileSize)
    {
        mCurrentFileNumber++;
        if(mCurrentFileNumber >= mFilesCout)
            mCurrentFileNumber = 0;

        name = mFileName + "_" + to_string(mCurrentFileNumber) + ".log";
        writeMode = std::ios::out;
    }

    ofstream fileStream(mPath + name, writeMode);
    if(!fileStream.is_open())
        return false;

    fileStream << date + '\t' + msg + "\n\r";

    fileStream.close();
    return true;
}

void MqLogSAV::fixMsg(string &msg)
{
    //test
//    std::cout << "Str " << msg.size() << "   >>" << msg << std::endl;

    if(msg.size() <= 5)
    {
        msg = "#";
        return;
    }
/* В ходе короткого исследования я выяснил,
 * что отлавливать порчу документа, продемонстрированную у Александра Федоровича
 * удается блокированием сообщения состоящего из одного символа
 */
    //\0 - 0
    //\t - 9
    //\n - 10
    //\r - 13
    auto isValid = [](const char& c){
        return c >= 32 || c == 0 || c == 9 || c == 10 || c == 13 || c < 0;
    };
    std::for_each(msg.begin(), msg.end(), [isValid, msg](char& c){
        if(!isValid(c))
        {
            c = '#';
        }
    });

}
