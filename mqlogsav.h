#ifndef MQLOGSAV_H
#define MQLOGSAV_H

#include <string>
#include <cstdint>

/*!
 * \brief Класс MqLogSAV
 * \details реализует логирование в несколько файлов,
 * поочереди, по мере их заполнения до предельного размера
 */
class MqLogSAV
{
public:
    /*!
     * \brief Конструктор MqLogSAV
     * \param имя файла без расширения
     * \param каталог с файлами логирования
     * \param предельное количество файлов логов
     * \param предельный размер файла в байтах
     */
    MqLogSAV(std::string fileName, std::string path, unsigned int fileCout = 2, unsigned int fileSize = 1024 * 1024);

    /*!
     * \brief log - метод записи лога в файл
     * \param message - сообщение
     * \return true - сообщение успешно записано, false - возникли ошибки
     */
    bool log(const std::string& message);

private:
    std::string mFileName;
    std::string mPath;
    //Число файлов для данного лога
    unsigned int mFilesCout = 1;
    //file size in bytes
    uint64_t mFileSize = 1024 * 1024;
    //Счетчик текущего номера файла
    unsigned int mCurrentFileNumber = 0;

private:
    void fixMsg(std::string& msg);
};

#endif // MQLOGSAV_H
